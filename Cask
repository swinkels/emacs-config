(source gnu)
(source melpa)

(depends-on "ace-jump-mode")    ;;to enable text-specific cursor jumps
(depends-on "ace-window")       ;;to enable window specific cursor jumps
(depends-on "ag")               ;;to provide a frontend for ag, the silver searcher
(depends-on "bm")               ;;to toggle visible bookmarks
(depends-on "cider")            ;;to have the Clojure IDE and REPL
(depends-on "buffer-move")      ;;to easily swap buffers in adjacent windows
(depends-on "diff-hl")          ;;to highlight uncommitted changes
(depends-on "iedit")            ;;to edit multiple regions in the same way simultaneously
(depends-on "expand-region")    ;;to increase the selected region by semantic units
(depends-on "fill-column-indicator")  ;; to graphically indicate the fill-column
(depends-on "flycheck")         ;;to enable on-the-fly error checking
(depends-on "grep")             ;;to edit grep buffers
(depends-on "helm")             ;;to provide incremental completion and selection narrowing
(depends-on "helm-ag")          ;;to provide Helm integration for the silver searcher
(depends-on "helm-gtags")       ;;to provide Helm integration for GNU Global tags
(depends-on "helm-projectile")  ;;to provide Helm integration for Projectile
(depends-on "hl-line+")         ;;to highlight the current line
(depends-on "hydra")            ;;to make bindings that stick around
(depends-on "jedi")             ;;to autocomplete Python
(depends-on "js2-mode")         ;;to better edit JavaScript mode
(depends-on "key-chord")        ;;to map commands to two-key combinations
(depends-on "magit")            ;;to better interact with Git
(depends-on "multiple-cursors") ;;to multiple selections simultaneously
(depends-on "paredit")          ;;to perform structured editing of S-expression data
(depends-on "project-explorer") ;;to view on directories and files in a sidebar
(depends-on "projectile")       ;;to manage and navigate projects in Emacs easily
(depends-on "slime")            ;;to have the Superior Lisp Interaction Mode
(depends-on "smex")             ;;to support M-x with ido-style fuzzy matching
(depends-on "undo-tree")        ;;to treat undo history as a tree
(depends-on "virtualenvwrapper")
(depends-on "wgrep-ack")        ;;to edit ack buffers
(depends-on "workgroups")       ;;to manage window configurations
(depends-on "yasnippet")        ;;to insert snippets (that is an understatement)
(depends-on "zenburn-theme")

(depends-on "sks-projects")     ;;to manage and navigate projects (builds on projectile)