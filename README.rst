This repo contains my personal Emacs configuration.

Certain parts of this configuration depend on external tools that are not part
of the Emacs ecosystem. For example, support for Python autocompletion requires
the availability of specific Python libraries. The Makefile in the root of the
repo can install (some of) these dependencies.

Although the configuration itself is independent of the OS, those external
dependencies are not. The aforementioned Makefile is being developed and tested
on Xubuntu 12.04 and 13.10. This does not mean the configuration will only work
on those Linux distributions, but other distributions or OS'es may require some
manual intervention.

Installation
------------

The following describes one way, but not the only way, to install my Emacs
configuration.

By default, Emacs looks for its configuration in directory ~/.emacs.d. It might
be that that directory already exists. If that is the case, please create a
backup of that directory first before you proceed. 

We assume directory ~/.emacs.d does not exist yet (or does not exist
anymore). Clone the configuration from Bitbucket to ~/.emacs.d::

  $> git clone https://bitbucket.org/swinkels/emacs-config.git ~/.emacs.d

Emacs package management
------------------------

The installation of Emacs packages that do not come with the standard version
of Emacs, is managed by Cask_, which has to be installed separately. To update
the dependencies to their latest version, issue the following command from the
root of this repo::

  $> cask update

.. _Cask: https://github.com/cask/cask
