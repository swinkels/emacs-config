(sks-deregister-projects)

;; Personal projects
;; =================

(sks-register-project "cmake-boost-test"
                      '((root-dir . "~/bitbucket.org/cmake-boost-test/")
                        (subdirs . "./ header-only/ static-library/ static-library-find-package/ dynamic-library/ dynamic-library-find-package")
                        (target-platforms . "debug release")
                        (rebuild-all-command . (lambda (target-platform) (concat "make --always-make build/" target-platform)))))

;; (sks-register-project "civ-tagger"
;;                       '((root-dir . "~/bitbucket.org/civ-tagger/")
;;                         (subdirs . "civ-tagger/ tests/")
;;                         (file-extensions . "-name '*.py'"))

;; Civolution projects through VPN
;; ===============================

(sks-register-project "FPSDK on OSX"
                      '((root-dir . "/pswinkels@192.168.214.109:~/repos/svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "ios-6.1")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "FPSDK for Panasonic LD4"
                      '((root-dir . "/pswinkels@192.168.214.7:svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "Panasonic")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))


(sks-register-project "FPSDK for Panasonic Pro4"
                      '((root-dir . "/pswinkels@192.168.213.128:svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "Panasonic-Pro4")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "FPSDK on Ubuntu 12.04"
                      '((root-dir . "/pswinkels@192.168.213.203:/mnt/external/repos/svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "android BCM972XX MT5369 NATIVE")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "MediaIdentifier on Ubuntu 12.04"
                      '((root-dir . "/pswinkels@192.168.213.203:/mnt/external/repos/svn.civolution.com/MediaIdentifier/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "NATIVE")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))
