;; (setq debug-on-error t)

;we set the GUI (elements) as soon as possible to reduce "flicker" at startup

;the following snippet is from Steve Yegge at
;
;  http://sites.google.com/site/steveyegge2/effective-emacs
;
;disable the scroll bars and toolbar
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

;Although Steve Yegge advises otherwise, I do not disable the menu bar in GUI
;mode as it provides me access to some not-so-often used functions. I do
;disable it in text mode as I do not seem able to memorize the keyboard
;shortcuts to the menu :)
(unless (display-graphic-p)
  (if (fboundp 'menu-bar-mode) (menu-bar-mode -1)))

;hide the welcome screen
(setq inhibit-splash-screen t)

;set several GUI properties in case Emacs can use a graphical display
(when (display-graphic-p)
  ;set the frame font; the value t for the third parameter means that all
  ;graphical frames should use this font

  ;this snippet is from http://www.reddit.com/r/emacs/comments/1xe7vr/check_if_font_is_available_before_setting/cfalidm
  (cond 
   ((find-font (font-spec :name "DejaVu Sans Mono"))
    (set-frame-font "DejaVu Sans Mono-9"))
   ((find-font (font-spec :name "Consolas"))
    (set-frame-font "Consolas-10:antialias=natural")))
 
  ;set the frame titlebar to the name of the current file - buffer
  (setq frame-title-format "%f - %b (%*)")
)

;initialize package management
(load "~/.emacs.d/sks-package-management.el")

;start Emacs in server mode unless there is an Emacs already running in server
;mode
(load-library "server")
(unless (server-running-p) (server-start))

;show column numbers
(setq column-number-mode t)

;use smooth scrolling
(setq scroll-conservatively 1)

;use cua-mode for superior rectangle handling
(cua-mode 1)
(cua-selection-mode 1)

(setq load-path (cons "~/.emacs.d/site-lisp" load-path))

;set the maximum line length and enable word wrap
(setq-default fill-column 79)
(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'python-mode-hook 'turn-on-auto-fill)

;enable syntax highlighting
(global-font-lock-mode 1)

;change default shell to zsh
(setq shell-file-name "zsh")

;reuse the path settings of your remote account when using tramp
(require 'tramp)
(add-to-list 'tramp-remote-path 'tramp-own-remote-path)

;enable the use of shell colors in Shell mode
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region (point-min) (point-max))
  (toggle-read-only))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;the following snippet is from Sebastian Wiesner at
;
;  http://www.lunaryorn.com/2015/04/29/the-power-of-display-buffer-alist.html
;
;make the compilation window come up at the bottom:
;  - press 'q' to 'bury the buffer' (or hide the window).
;  - execute display-buffer to redisplay the compilation buffer
(add-to-list 'display-buffer-alist
             `(,(rx bos "*compilation*" eos)
               (display-buffer-reuse-window
                display-buffer-in-side-window)
               (reusable-frames . visible)
               (side            . bottom)
               (window-height   . 0.3)))

;for more efficient typing, use C-x C-m or C-c C-m instead of M-x (note that this
;is only more efficient when you have swapped Caps-Lock and Control)
(global-set-key "\C-x\C-m" 'execute-extended-command)

;;to support M-x with ido-style fuzzy matching
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)

(global-set-key [f4]       'next-error)
(global-set-key [C-f4]     'kill-buffer)

(global-set-key [f5]       'call-last-kbd-macro)
(global-set-key [S-f5]     'apply-macro-to-region-lines)

;use dired-x for several handy dired extensions
(require 'dired-x)

;use windmove to facilitate movement from window to window
(require 'windmove)
;use the default modifier for the cursor keys, that is, shift
(windmove-default-keybindings)

;;use a smarter than average occur, from http://oremacs.com/2015/01/26/occur-dwim/
(defun occur-dwim ()
  "Call `occur' with a sane default."
  (interactive)
  (push (if (region-active-p)
            (buffer-substring-no-properties
             (region-beginning)
             (region-end))
          (thing-at-point 'symbol))
        regexp-history)
  (call-interactively 'occur))

(global-set-key (kbd "M-s o") 'occur-dwim)

;use uniquify to use more telling buffer names when multiple buffers belong to
;different files with the same name
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward)

;define function to toggle the current window split (only works for two
;windows)
;from http://www.emacswiki.org/emacs/ToggleWindowSplit
(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
	(delete-other-windows)
	(let ((first-win (selected-window)))
	  (funcall splitter)
	  (if this-win-2nd (other-window 1))
	  (set-window-buffer (selected-window) this-win-buffer)
	  (set-window-buffer (next-window) next-win-buffer)
	  (select-window first-win)
	  (if this-win-2nd (other-window 1))))))

;key binding to join the current line with the next
(global-set-key (kbd "M-j") (lambda () (interactive) (join-line -1)))
;source http://whattheemacsd.com/key-bindings.el-03.html

;source http://emacsredux.com/blog/2013/05/22/smarter-navigation-to-the-beginning-of-a-line/
(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;; remap C-a to `smarter-move-beginning-of-line'
(global-set-key [remap move-beginning-of-line]
                'smarter-move-beginning-of-line)

;use ido package for (among others) an improved file and buffer selection
;
;we load ido early on as we have seen one package, viz. pylookup, that does not
;use it when its activation code is placed after that of ido
(require 'ido)

(ido-mode t)
(ido-everywhere t)

(setq ido-enable-flex-matching t)
;disable the use of the filename at point due to the large number of false
;positives: ido considers a lot of text to be a filename
;; (setq ido-use-filename-at-point t)

;; disable the search for files in other directories when you do C-x C-f
(setq ido-auto-merge-work-directories-length -1)

;use flyspell to mark misspelled words
(add-hook 'text-mode-hook
          '(lambda () (flyspell-mode 1))
)

;auto-indent after return
(add-hook 'prog-mode-hook
          '(lambda () (local-set-key (kbd "C-m") 'newline-and-indent)))

;use flyspell to mark misspelled words in comments and strings
(add-hook 'prog-mode-hook 'flyspell-prog-mode)

;update the copyright information right before a save
(add-hook 'prog-mode-hook
          '(lambda ()
             (add-hook 'before-save-hook 'copyright-update nil t)))

;; ;use package ropemacs for Python autocomplete and refactoring functionality
;; (add-to-list 'load-path "~/repos/github.com/Pymacs")
;; (require 'pymacs)
;; ;do not install the ropemacs shortcut keys by default: this also rebinds M-/ to
;; ;use rope-code-assist although I prefer the default binding and functionality
;; ;of dabbrev-expand
;; (setq ropemacs-enable-shortcuts nil)
;; (pymacs-load "ropemacs" "rope-")
;; (add-hook 'python-mode-hook
;;           '(lambda ()
;;              (define-key python-mode-map "\C-cg" 'rope-goto-definition)
;;              (define-key python-mode-map "\C-cu" 'rope-pop-mark)
;;              (define-key python-mode-map "\C-cd" 'rope-show-doc)
;;              (define-key python-mode-map "\C-cf" 'rope-find-occurrences)
;;              (define-key python-mode-map "\M-?" 'rope-lucky-assist)))

(global-set-key [S-f7] 'kill-compilation)

(let ((frontpage-file (concat "~/.emacs.d/frontpage-" system-name ".el")))
  (if (file-exists-p frontpage-file)
    (load-file frontpage-file)))

;for the following snippets I am indebted to Bozhidar Ivanov Batsov, mostly at
;http://emacsredux.com/

;automatically insert the closing paired character
;(electric-pair-mode +1)

(defun smart-open-line ()
  "Insert an empty line after the current line.
Position the cursor at its beginning, according to the current mode."
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent))

(defun smart-open-line-above ()
  "Insert an empty line above the current line.
Position the cursor at it's beginning, according to the current mode."
  (interactive)
  (move-beginning-of-line nil)
  (newline-and-indent)
  (forward-line -1)
  (indent-according-to-mode))

(global-set-key (kbd "M-o") 'smart-open-line)
(global-set-key (kbd "M-O") 'smart-open-line-above)

(defun copy-current-line()
  "Copy the current line below the line that contains point."
  (interactive)
  (let ((column (current-column))
        (line (buffer-substring (line-beginning-position) (line-end-position))))
    (kill-new line)
    (move-end-of-line nil)
    (newline)
    (insert line)
    (move-to-column column)))

(key-chord-define-global "jc" 'copy-current-line)

(defun kill-current-line()
  "Kill current line."
  (interactive)
  (let ((column (current-column)))
    (kill-whole-line)
    (move-to-column column)))

(key-chord-define-global "jk" 'kill-current-line)

;inspired by http://oremacs.com/2015/01/29/more-hydra-goodness/

(defhydra hydra-window (:color amaranth)
  "window"
  ("h" buf-move-left)
  ("j" buf-move-down)
  ("k" buf-move-up)
  ("l" buf-move-right)
  ("a" ace-window "ace" :color blue)
  ("s" ace-swap-window "swap")
  ("d" ace-delete-window "del")
  ("f" ace-maximize-window "max" :color blue)
  ("q" nil "cancel"))

(key-chord-define-global "yy" 'hydra-window/body)

(defun mark-current-line()
  "Mark the current line."
  (interactive)
  (beginning-of-line)
  (unless mark-active
    (push-mark)
    (set 'mark-active t))
  (next-line)
  (beginning-of-line))
  
(key-chord-define-global "jl" 'mark-current-line)

;settings for Semantic & EDE (to provide semantic completion)
;(global-ede-mode 1)

;define the EDE C++ roots
(let ((ede-cpp-roots-file (concat "~/.emacs.d/ede-cpp-roots-" system-name ".el")))
  (if (file-exists-p ede-cpp-roots-file)
    (load-file ede-cpp-roots-file)))

;; (semantic-mode 1)
;; (global-semantic-idle-summary-mode 1)

;use spaces instead of tabs to indent lines - why are tabs the default?
(setq-default indent-tabs-mode nil)

;settings for C/C++ mode
(add-hook 'c-mode-common-hook
	  '(lambda ()
             (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)
	     (c-set-style "bsd")
	     (setq c-basic-offset 4)
	     (setq tab-width 4)
	     (local-set-key (kbd "RET") 'newline-and-indent)))

;set the regexp so Emacs recognizes the error lines in the output of 'ctest -V'
;in compilation mode
(add-hook 'compilation-mode-hook
          '(lambda ()
             (add-to-list 'compilation-error-regexp-alist-alist '(ctest "^[0-9]+: \\([^\(\n]+\\)\(\\([0-9]+\\)\):" 1 2))
             (add-to-list 'compilation-error-regexp-alist 'ctest)))

;let the compilation window scroll to the first error
(setq compilation-scroll-output 'first-error)

;add ObjectiveC / ObjectiveC++ file names to the mode list
(setq auto-mode-alist
      (append
       '(("\\.m\\'" . objc-mode))
       '(("\\.mm\\'" . objc-mode))
       auto-mode-alist))

;settings for Python mode

;I used to use python-mode, which can be found at
;
;  https://launchpad.net/python-mode
;
;March 17, 2012 I upgraded to the latest version, which appears to offer
;everything and the kitchen sink. As documentation was sparse and I had some
;useability issues with it, I decided to use the standard Python mode.

;use package auto-complete for graphical completion support
;; (add-to-list 'load-path "~/.emacs.d/externals/popup-el")
;; (add-to-list 'load-path "~/.emacs.d/externals/auto-complete")
;; (require 'auto-complete)
;; (add-to-list 'ac-dictionary-directories "~/.emacs.d/externals/auto-complete/dict")
;; (require 'auto-complete-config)
;; (ac-config-default)
;; ;do not start autocomplete automatically as it tends to get in the way: we use
;; ;a key binding instead
;; (setq ac-auto-start nil)
;; (global-set-key (kbd "C-.") 'auto-complete)
;; ;enable Rope to auto-complete in Python mode
;; (ac-ropemacs-initialize)

;; (add-hook 'c-mode-common-hook
;;           '(lambda () (setq ac-sources (cons ac-source-semantic-raw (cons ac-source-semantic ac-sources)))))

;pylint does not like trailing whitespace, so we remove it
(add-hook 'python-mode-hook
          '(lambda ()
             (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)))

;use package sks-python for some Python related functionality
(load "~/.emacs.d/my-lisp/sks-python.el")

(add-hook 'python-mode-hook
          '(lambda ()
             (define-key python-mode-map "\C-t" 'sks-python-toggle-source)))

;use package sks-python-buildout to find the commands installed by buildout

(load "~/.emacs.d/my-lisp/sks-find-command.el")

(add-hook 'python-mode-hook
          '(lambda () (sks-python-set-compile-to-buildout-test))
)

(global-set-key [C-f7] 'sks-python-set-compile-to-buildout-pylint)

;use package sks-org-mode for to setup Org mode
(load "~/.emacs.d/my-lisp/sks-org-mode.el")

(defun switch-to-eshell()
  (interactive)
  (let ((eshell-window (get-buffer-window "*eshell*")))
    (if eshell-window
	(select-window eshell-window)
      (let ((eshell-buffer (get-buffer "*eshell*")))
	(if eshell-buffer
	    (switch-to-buffer "*eshell*")
	  (eshell))))))

;bind function keys to the shell
(defun switch-to-shell()
  (interactive)
  (unless (get-buffer "*shell*")
    (shell))
  (switch-to-buffer "*shell*"))

(global-set-key [f12]   'switch-to-eshell)
(global-set-key [S-f12] 'switch-to-shell)

;move through the changes in a *vc-diff* window from any window
(require 'log-edit)

(defun navigate-hunk(navigate-func)
  "Move to another hunk in buffer *vc-diff* using function NAVIGATE_FUNC.
This function moves point to buffer *vc-diff*, if it exists, and
calls NAVIGATE_FUNC to move to another hunk. Then it moves point
back to the original buffer. This function does nothing if
*vc-diff* does not exist."
  (let ((vc-diff-buffer "*vc-diff*"))
    (let ((original-buffer (current-buffer)))
      (unless (eq (get-buffer vc-diff-buffer) nil)
	(switch-to-buffer-other-window vc-diff-buffer)
	(condition-case err (funcall navigate-func)
	  (error (message "%s" (error-message-string err)))))
      (switch-to-buffer-other-window original-buffer))))

(defun previous-hunk()
  "Move to the previous hunk in buffer *vc-diff* and return to the current buffer."
  (interactive)
  (navigate-hunk 'diff-hunk-prev))

(defun next-hunk()
  "Move to the previous hunk in buffer *vc-diff* and return to the next buffer."
  (interactive)
  (navigate-hunk 'diff-hunk-next))

(define-key log-edit-mode-map [backtab] 'previous-hunk)
(define-key log-edit-mode-map [tab] 'next-hunk)

;when you press "v" in diff-mode, call vc-next-action as in buffer *vc-dir*
(require 'diff-mode)
(define-key diff-mode-map "v" 'vc-next-action)

;maintain a list of recently visited files
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-saved-items 20)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(cua-mode t nil (cua-base))
 '(custom-safe-themes
   (quote
    ("427234e4b45350b4159575f1ac72860c32dce79bb57a29a196b9cfb9dd3554d9" default)))
 '(electric-pair-inhibit-predicate (quote electric-pair-conservative-inhibit))
 '(imenu-create-index-p nil)
 '(org-agenda-use-time-grid nil)
 '(org-link-frame-setup
   (quote
    ((vm . vm-visit-folder-other-frame)
     (gnus . gnus-other-frame)
     (file . find-file))))
 '(org-return-follows-link t)
 '(org-time-clocksum-format
   (quote
    (:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t)))
 '(projectile-switch-project-action (quote projectile-vc))
 '(projectile-tags-command "etags -R %s %s")
 '(ps-n-up-printing 1)
 '(ps-paper-type (quote a4))
 '(py-pychecker-command "pyflap8")
 '(py-pychecker-command-args (quote ("")))
 '(py-shell-name "ipython")
 '(python-check-command "pyflap8")
 '(ropemacs-max-doc-buffer-height 0)
 '(tool-bar-mode nil)
 '(wg-morph-on nil)
 '(windmove-window-distance-delta 2))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(rst-level-1-face ((t (:background "darkorange"))) t)
 '(rst-level-2-face ((t (:background "darkorange"))) t))
