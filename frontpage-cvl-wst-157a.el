(sks-deregister-projects)

;; Civolution projects
;; ===================

(sks-register-project "EmAFPSDK on Ubuntu 12.04"
                      '((root-dir . "/mnt/external/repos/svn.civolution.com/EmAFPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "android BCM972XX MT5369 NATIVE Panasonic Panasonic-Pro4")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "FPSDK on Ubuntu 12.04"
                      '((root-dir . "/mnt/external/repos/svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "android BCM972XX MT5369 NATIVE Panasonic Panasonic-Pro4")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "EmAFPSDK on OSX (Ardjan)"
                      '((root-dir . "/pswinkels@192.168.214.39:repos/svn.civolution.com/EmAFPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "ios-6.1 iossim-6.1")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "FPSDK on OSX (Ardjan)"
                      '((root-dir . "/pswinkels@192.168.214.39:repos/svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "ios-6.1")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "FPSDK on OSX (Stepan)"
                      '((root-dir . "/pswinkels@192.168.214.109:repos/svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "ios-6.1")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "MediaIdentifier on Ubuntu 12.04"
                      '((root-dir . "/mnt/external/repos/svn.civolution.com/MediaIdentifier/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "NATIVE")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))

(sks-register-project "FPSDK for Panasonic LD4"
                      '((root-dir . "/pswinkels@192.168.214.131:svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "Panasonic")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))


(sks-register-project "FPSDK for Panasonic Pro4"
                      '((root-dir . "/pswinkels@192.168.213.128:svn.civolution.com/FPSDK/")
                        (subdirs . "Source/ Modules/")
                        (target-platforms . "Panasonic-Pro4")
                        (rebuild-all-command . (lambda (target-platform) (concat "cmake -DTARGET_PLATFORMS=" target-platform " -P build.cmake")))))
