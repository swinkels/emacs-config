BITBUCKET_REPOS_DIR=~/repos/bitbucket.org
GITHUB_REPOS_DIR=~/repos/github.com

ifeq ($(OS),Windows_NT)
	ALL=install-jedi install-ropemacs install-sks-projects
	CMD=cmd.exe /C
else
	ALL=install-ag install-aspell install-jedi install-ropemacs install-sks-projects
	SUDO=sudo
endif

all: $(ALL)

install-ag:
	$(SUDO) apt-get install silversearcher-ag

install-aspell:
	$(SUDO) apt-get install aspell aspell-nl

# to autocomplete Python
install-jedi: 
	$(SUDO) pip install jedi
	$(SUDO) pip install epc

# to perform Python refactorings
install-ropemacs: install-pymacs
	$(SUDO) pip install ropemacs

install-pymacs: install-directory-structure
	- $(SUDO) pip uninstall Pymacs
	- rm -rf $(GITHUB_REPOS_DIR)/Pymacs
	cd $(GITHUB_REPOS_DIR) ; git clone git://github.com/pinard/Pymacs.git
	# The next command comes from the Makefile that Pymacs provides. That
	# part of the Makefile does not work with Windows and/or Cygwin so we
	# execute the build step ourselves.
	cd $(GITHUB_REPOS_DIR)/Pymacs ; $(CMD) python pppp -C ppppconfig.py Pymacs.py.in pppp.rst.in pymacs.el.in pymacs.rst.in contrib tests
	cd $(GITHUB_REPOS_DIR)/Pymacs ; $(SUDO) pip install .

install-sks-projects: install-directory-structure
	- rm -rf $(BITBUCKET_REPOS_DIR)/sks-projects
	cd $(BITBUCKET_REPOS_DIR) ; hg clone https://swinkels@bitbucket.org/swinkels/sks-projects
	cask link sks-projects $(BITBUCKET_REPOS_DIR)/sks-projects

install-directory-structure:
	- mkdir $(BITBUCKET_REPOS_DIR)
	- mkdir $(GITHUB_REPOS_DIR)
