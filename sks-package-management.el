(require 'cask "~/repos/github.com/cask/cask.el")
(cask-initialize)

;;the following way of customizing packages was read from
;;http://www.emacswiki.org/emacs/ELPA

(eval-after-load "ace-window-autoloads"
  '(progn
     (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))))

(eval-after-load "ace-jump-mode-autoloads"
  '(progn
     (autoload
       'ace-jump-mode
       "ace-jump-mode"
       "Emacs quick move minor mode"
       t)
     (define-key global-map (kbd "C-0") 'ace-jump-mode)))

(eval-after-load "bm-autoloads"
  '(progn
     (global-set-key [C-f2] 'bm-toggle)
     (global-set-key [f2]   'bm-next)
     (global-set-key [S-f2] 'bm-previous)))

(eval-after-load "cider-autoloads"
  '(progn
     (add-hook 'cider-repl-mode-hook 'paredit-mode)))

(eval-after-load "diff-hl-autoloads"
  '(progn
    (global-diff-hl-mode)))

(eval-after-load "expand-region-autoloads"
  '(progn
    (global-set-key (kbd "C-=") 'er/expand-region)))

(eval-after-load "fill-column-indicator-autoloads"
  '(progn
    (fci-mode)))

(eval-after-load "flycheck-autoloads"
  '(progn
     (global-flycheck-mode)))

(eval-after-load "helm-gtags-autoloads"
  '(progn
     ;the following setup is adapted from http://tuhdo.github.io/c-ide.html#sec-1
     (setq helm-gtags-ignore-case t)
     (setq helm-gtags-auto-update t)

     (require 'helm-gtags)
     (add-hook 'c-mode-hook 'helm-gtags-mode)
     (add-hook 'c++-mode-hook 'helm-gtags-mode)

     (define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
     (define-key helm-gtags-mode-map (kbd "M-*") 'helm-gtags-pop-stack)))

(eval-after-load "helm-projectile-autoloads"
  '(progn
     (require 'helm-projectile)
     (helm-projectile-on)))

(eval-after-load "hl-line+-autoloads"
  '(progn
     (toggle-hl-line-when-idle 1)))

(eval-after-load "jedi-autoloads"
  '(progn
     (add-hook 'python-mode-hook 'jedi:setup)
     (setq jedi:setup-keys t)
     (setq jedi:complete-on-dot t)))

(eval-after-load "js2-mode-autoloads"
  '(progn
     (autoload 'js2-mode "js2-mode" nil t)
     (add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))))

(eval-after-load "helm-autoloads"
  '(progn
     (require 'helm-config)
     ;split the current window in two and show the helm window in the bottom
     ;part
     (setq helm-split-window-in-side-p t)
     (setq helm-split-window-default-side 'below)))

(eval-after-load "key-chord-autoloads"
  '(progn
     (key-chord-mode 1)
     (setq key-chord-two-keys-delay .2)))

(eval-after-load "multiple-cursors-autoloads"
  '(progn
     (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
     (global-set-key (kbd "C->") 'mc/mark-next-like-this)
     (global-set-key (kbd "C-*") 'mc/mark-all-like-this)))

(eval-after-load "workgroups-autoloads"
  '(progn
     (require 'workgroups)
     ;change the prefix (from C-z)
     (setq wg-prefix-key (kbd "C-c w"))
     ;activate workgroup-mode
     (workgroups-mode 1)
     ;do not activate the first saved workgroup
     (setq wg-switch-on-load nil)
     ;load saved workgroups
     (wg-load "~/.emacs.d/saved-workgroups")))

(eval-after-load "jedi-autoloads"
  '(progn
     (add-hook 'python-mode-hook 'jedi:setup)
     (setq jedi:setup-keys t)
     (setq jedi:complete-on-dot t)))

(eval-after-load "project-explorer"
  '(progn
     (setq pe/side 'right)))

(eval-after-load "projectile-autoloads"
  '(progn
     (projectile-global-mode)))

(eval-after-load "sks-projects-autoloads"
  '(progn
     (global-set-key [f7] 'sks-recompile)
     (add-hook 'python-mode-hook
               '(lambda () (local-set-key [C-f7] 'sks-py-project-execute-test)))))

(eval-after-load "slime-autoloads"
  '(progn
     (setq inferior-lisp-program "/usr/bin/clisp") ; your Lisp system
     (require 'slime)
     (slime-setup '(slime-fancy))))

(eval-after-load "undo-tree-autoloads"
  '(progn
     (global-undo-tree-mode)))

(eval-after-load "yasnippet-autoloads"
  '(progn
     (yas-global-mode 1)))

(eval-after-load "zenburn-theme-autoloads"
  '(progn
     (when (display-graphic-p)
       (load-theme 'zenburn t))))

;The following Lisp code makes sure that the el-get packages I use are
;installed at Emacs startup. I have copied it from the EmacsWiki at
;
;  http://www.emacswiki.org/emacs/el-get

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")

(setq el-get-sources
      '(el-get
        cmake-mode   ;;to provide syntax highlighting and indentation
        etags-select ;;to select from multiple tags
        ))

(el-get 'sync)

;;add cmake listfile names to the mode list.
(setq auto-mode-alist
      (append
       '(("CMakeLists\\.txt\\'" . cmake-mode))
       '(("\\.cmake\\'" . cmake-mode))
       auto-mode-alist))
