;;; sks-python.el --
;; Copyright (c) 2009 Pieter Swinkels
;;
;; Author: Pieter Swinkels <swinkels dot pieter at yahoo dot com>
;; Keywords: Python
;; Version: 1.0
;;
;; This file is not part of GNU Emacs
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun retrieve-toggle-filename(filename)
  (if (string-match "_tests.py$" filename)
      (replace-match ".py" t t filename)
    (concat (file-name-sans-extension filename) "_tests.py"))
  )

(defun sks-python-toggle-source()
  (interactive)
  (find-file
   (concat
    (file-name-directory (buffer-file-name))
    (retrieve-toggle-filename (file-name-nondirectory (buffer-file-name)))))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun kill-test-buffers()
  (set-buffer-modified-p nil)
  (kill-buffer "file_tests.py")
  (kill-buffer "file.py"))

(defun test-toggle-source-finds-the-python-tests-file()
  (save-excursion
    (find-file "file.py")
    (sks-python-toggle-source)
    (assert (string= (file-name-nondirectory (buffer-file-name)) "file_tests.py"))
    (kill-test-buffers))
  )

(defun test-toggle-source-finds-the-python-tests-file-in-the-right-directory()
  (save-excursion
    (find-file "file.py")
    (let ((current-directory (file-name-directory (buffer-file-name))))
          (sks-python-toggle-source)
          (assert (string= current-directory (file-name-directory (buffer-file-name)))))
    (kill-test-buffers))
  )

(defun test-toggle-source-finds-the-python-file()
  (save-excursion
    (find-file "file_tests.py")
    (sks-python-toggle-source)
    (assert (string= (file-name-nondirectory (buffer-file-name)) "file.py"))
    (kill-test-buffers))
  )

(defun test-toggle-source-finds-the-python-file-in-the-right-directory()
  (save-excursion
    (find-file "file_tests.py")
    (let ((current-directory (file-name-directory (buffer-file-name))))
          (sks-python-toggle-source)
          (assert (string= current-directory (file-name-directory (buffer-file-name)))))
    (kill-test-buffers))
  )
