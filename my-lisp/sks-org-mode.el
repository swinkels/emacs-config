(require 'org)

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

;create the key bindings to insert and follow links that have Org syntax in all
;buffers (and not only those in Org mode)
(global-set-key "\C-c L" 'org-insert-link-global)
(global-set-key "\C-c o" 'org-open-at-point-global)

;the following two commands setup persistence of the org-mode clock: the entire
;clock history is saved when emacs closes
(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)

;include the current clocking task in clock reports
(setq org-clock-report-include-clocking-task t)

;do not use the Emacs Calendar's diary but our own org-mode diary file
(setq org-agenda-include-diary nil)
(setq org-agenda-diary-file "~/bitbucket.org/org-mode-docs/diary.org")
