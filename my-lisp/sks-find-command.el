(defun absolute-dir(directory)
  (if directory
      (file-truename directory)
    (file-truename "~/"))
)

(defun is-allowed(relative-command absolute-highest-dir)
  (let ((length-absolute-highest-dir (length absolute-highest-dir))
	(absolute-dir (file-name-directory (file-truename relative-command))))
    (if (<= length-absolute-highest-dir (length absolute-dir))
	(equal absolute-highest-dir
	       (substring absolute-highest-dir 0 length-absolute-highest-dir))
      nil))
)

(defun sks-find-command(relative-command &optional highest-dir)
  (let ((absolute-highest-dir (absolute-dir highest-dir)))
    (while (and (is-allowed relative-command absolute-highest-dir)
		(not (file-exists-p relative-command)))
      (setq relative-command (concat "../" relative-command)))
    (if (not (is-allowed relative-command absolute-highest-dir))
	(setq relative-command ""))
    relative-command)
)

(defun sks-find-and-set-compile-command(relative-command &optional parameters)
  (let ((found-relative-command (sks-find-command relative-command)))
    (if (not (equal found-relative-command ""))
	(progn
	  (if parameters
	      (setq parameters (concat " " parameters)))
	  (set 'compile-command (concat found-relative-command parameters)))))
)

;; We make the following commands interactive to be able to bind themt to a
;; key. If we do not make them interactive, Emacs gives us a message like the
;; following when the user presses the bound key:
;;
;;   Wrong type argument: commandp, sks-python-set-compile-to-buildout-test

(defun sks-python-set-compile-to-buildout-test()
  (interactive)
  (set 'compile-command
       (sks-find-and-set-compile-command "../bin/test" "--with-coverage"))
)

(defun sks-python-set-compile-to-buildout-pylint()
  (interactive)
  (set 'compile-command (sks-find-and-set-compile-command
			 "../bin/pylint"
			 (concat "-f parseable -d I "
				 (file-name-nondirectory buffer-file-name))))
)
